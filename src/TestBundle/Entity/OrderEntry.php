<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use APY\DataGridBundle\Grid\Mapping as GRID;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
 
/**
 * OrderEntry
 *
 * @GRID\Source(columns="id, orderId, idFlux, marketplace, orderAmount, orderShipping, orderCommission", filterable=false)
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TestBundle\Entity\OrderEntryRepository")
 * @UniqueEntity("orderId", message="La valeur de orderId doit être unique.")
 */
class OrderEntry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", length=20)
     */
    private $orderId;

    /**
     * @var integer
     *
     * @ORM\Column(name="idFlux", type="integer")
     */
    private $idFlux;

    /**
     * @var string
     *
     * @ORM\Column(name="marketplace", type="string", length=255)
     */
    private $marketplace;

    /**
     * @var string
     *
     * @ORM\Column(name="order_amount", type="decimal")
     */
    private $orderAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="order_shipping", type="decimal")
     */
    private $orderShipping;

    /**
     * @var string
     *
     * @ORM\Column(name="order_commission", type="decimal")
     */
    private $orderCommission;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return OrderEntry
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set idFlux
     *
     * @param integer $idFlux
     * @return OrderEntry
     */
    public function setIdFlux($idFlux)
    {
        $this->idFlux = $idFlux;

        return $this;
    }

    /**
     * Get idFlux
     *
     * @return integer 
     */
    public function getIdFlux()
    {
        return $this->idFlux;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return OrderEntry
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set orderAmount
     *
     * @param string $orderAmount
     * @return OrderEntry
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return string 
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * Set orderShipping
     *
     * @param string $orderShipping
     * @return OrderEntry
     */
    public function setOrderShipping($orderShipping)
    {
        $this->orderShipping = $orderShipping;

        return $this;
    }

    /**
     * Get orderShipping
     *
     * @return string 
     */
    public function getOrderShipping()
    {
        return $this->orderShipping;
    }

    /**
     * Set orderCommission
     *
     * @param string $orderCommission
     * @return OrderEntry
     */
    public function setOrderCommission($orderCommission)
    {
        $this->orderCommission = $orderCommission;

        return $this;
    }

    /**
     * Get orderCommission
     *
     * @return string 
     */
    public function getOrderCommission()
    {
        return $this->orderCommission;
    }
	
	
	public function getArray(){
		return array(
			'order_id'=>$this->orderId,
			'idFlux'=>$this->idFlux,
			'order_amount'=>$this->orderAmount,
			'order_shipping'=>$this->orderShipping,
			'order_commission'=>$this->orderCommission,
			'marketplace'=>$this->marketplace
		);	
	}
}
