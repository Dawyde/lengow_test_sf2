<?php

namespace TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TestBundle\Entity\OrderEntry;
use TestBundle\Form\OrderEntryType;
use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Component\HttpFoundation\Response;

class TestController extends Controller
{
    public function importAction()
    {
		$lengow_test = $this->get('lengow_test');
		$lengow_test->importXML();
		return new Response("Fichier XML importé !");
    }
    public function viewAction()
    {
		$source = new Entity('TestBundle:OrderEntry');

        // Get a Grid instance
        $grid = $this->get('grid');

        // Attach the source to the grid
        $grid->setSource($source);

        // Return the response of the grid to the template
        return $grid->getGridResponse('TestBundle:Test:view.html.twig');
    }
    public function createAction()
    {
		$orderEntry = new OrderEntry();
			
		//On crée le formulaire
		$form = $this->createForm(new OrderEntryType(), $orderEntry);
		
		
		$request = $this->get('request');
		if ($request->getMethod() == 'POST') {
			$form->bind($request);
			//On vérifie si le formulaire est valide
			if ($form->isValid()) {
				$em = $this->getDoctrine()->getManager();
				$em->persist($orderEntry);
				$em->flush();
				return $this->redirect($this->generateUrl('test_view'));
			}
		}

        // Return the response of the grid to the template
        return $this->render('TestBundle:Test:create.html.twig', array(
			'form'=>$form->createView()
		));
    }
	
    public function apiListAction()
    {
		//On récupère le repository
		$rep = $this->get('doctrine')->getRepository("TestBundle:OrderEntry");
		
		//On récupère l'encodeur
		$request = $this->get('request');
		$encoder = $this->get(($request->query->has('yaml') && $request->query->get('yaml')==1)?'lengow_test.yaml_encoder':'lengow_test.json_encoder');
		
		//On crée notre objet
		$entries = $rep->findAll();
		$data = array();
		foreach($entries as $orderEntry){
			$data[] = $orderEntry->getArray();
		}
		
		//On envoie la réponse
		return new Response($encoder->encode($data));
    }
	
    public function apiGetAction($orderId)
    {
		$rep = $this->get('doctrine')->getRepository("TestBundle:OrderEntry");
		$request = $this->get('request');
		$encoder = $this->get(($request->query->has('yaml') && $request->query->get('yaml')==1)?'lengow_test.yaml_encoder':'lengow_test.json_encoder');
		
		$orderEntry = $rep->findOneBy(array('orderId'=>$orderId));
		$data = array();
		if($orderEntry)	$data = $orderEntry->getArray();
		else $data['error'] = 'Commande introuvable';
		
		
		return new Response($encoder->encode($data));
    }
}
