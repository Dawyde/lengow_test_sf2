<?php
namespace TestBundle\Services\Encoders;
use Symfony\Component\Yaml\Yaml;

class YAMLEncoder extends AbstractEncoder{
	
	public function encode($object, $inline=5){
		return Yaml::dump($object, $inline);
	}
	
}