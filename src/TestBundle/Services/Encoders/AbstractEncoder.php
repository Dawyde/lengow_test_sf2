<?php
namespace TestBundle\Services\Encoders;

abstract class AbstractEncoder{
	abstract public function encode($object);
	
}