<?php
namespace TestBundle\Services\Encoders;

class JSONEncoder extends AbstractEncoder{
	
	public function encode($object){
		return json_encode($object);
	}
	
}