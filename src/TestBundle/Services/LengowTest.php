<?php
namespace TestBundle\Services;

use TestBundle\Entity\OrderEntry;

class LengowTest{
	
	public function __construct($em, $logger, $url_orders){
		$this->em = $em;
		$this->logger = $logger;
		$this->url_orders = $url_orders;
	}
	
	public function importXML(){
		$orderRepository = $this->em->getRepository("TestBundle:OrderEntry");
		$orderMetadata = $this->em->getClassMetadata("TestBundle:OrderEntry");
		
		
		$document = new \DOMDocument();
		$document->loadXML($this->loadURL($this->url_orders));
		
		$orders = $document->getElementsByTagName("orders");
		
		if($orders->length == 0) return false;
		
		//On liste les commandes
		foreach($orders->item(0)->childNodes as $orderNode){
			$newOrder =  new OrderEntry();
			//On importe les valeurs dans l'entit�
			$this->import($orderMetadata, $newOrder, $orderNode);
			
			//On v�rifie si l'entit� existe d�j�
			$order = $orderRepository->findBy(array('orderId'=>$newOrder->getOrderId()));
			if($order == null){
				//Si l'entit� n'existe pas on l'ajoute
				$this->em->persist($newOrder);
			}
		}
		
		//On flush les nouvelles entr�es
		$this->em->flush();
	}
	
	private function loadURL($url){
		$this->logger->info("Chargement de ".$url);
		return file_get_contents($url);
	}
	
	private function import($metadata, $entity, $node){
		foreach($node->childNodes as $item){
			//On v�rifie si le tag est mapp� dans l'entit�
			if(!array_key_exists($item->tagName, $metadata->fieldNames)) continue;
			$field = $metadata->fieldMappings[$metadata->fieldNames[$item->tagName]];
			//On v�rifie que le champ doit �tre pris en compte
			if($field['type'] != 'integer' && $field['type'] != 'decimal' && $field['type'] != 'text' && $field['type'] != 'string') continue;
			//On met la valeur dans l'entit�
			call_user_func(array(&$entity, "set".$field['fieldName']), $item->nodeValue);
		}
	}
}